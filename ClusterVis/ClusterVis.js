/**
 * USFCA Data Visualization Research - ClusterVis Implementation
 * Copyright (C) 2018  Martino Kuan
 *
 * This file is part of USFCA Data Visualization Research - ClusterVis
 * Implementation.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Width and height are the same for this radial graph.
const canvasSideLength = 400;
const padding = 20;

const svgSideLength = canvasSideLength - padding;
const graphOrigin = canvasSideLength / 2;

let svg = d3.select("#mainCanvas")

const acceptedLabelTitles = ["Category", "Research", "Area"];
const acceptedValueTitles = ["Qt Research", "Qt Publications", "Qt Journals", "Qt Books", "Qt Proceedings", "Degree"];

let titlesUsed = ["Category", "Qt Publications", "Qt Journals", "Qt Books", "Qt Proceedings"];

const ringList = d3.select("#ringList");

let radius;
let elementRadius;

function main() {
  svg.attr("width", canvasSideLength)
      .attr("height", canvasSideLength);

  let dataSetSelection = d3.select("#dataSetSelection")
      .on("change", handleOnDataSetSelectionChange);

  drawClusterVis();
}

function drawClusterVis() {
  let dataFilePath = d3.select("#dataSetSelection")
      .property("value");

  d3.json(dataFilePath)
      .then((data) => {
        let nodesInfo = data.nodes;

        let labelTitles = nodesInfo.labelTitle;
        let valueTitles = nodesInfo.valueTitle;
        
        let dataNodes = nodesInfo.dataNodes;

        d3.select("#centerRadiusRange")
            .on("change", handleOnCenterRadiusRangeChange);

        d3.select("#sortSelection")
            .on("change", handleOnSortSelectionChange);

        updateSortSelectionValues();

        d3.select("#addRingButton")
            .on("click", handleOnClickAddRingButton);
        d3.select("#removeRingButton")
            .on("click", handleOnClickRemoveRingButton);

        // Sort the values using the default value.
        sortElements(titlesUsed[0]);

        // Render each ring.
        let ringsGroup = svg.append("g");
        updateRings();

        // Define the scales for the label (categorical) attributes.
        let scales = {};
        for (let title of acceptedLabelTitles) {
          let labelTitlePosition = labelTitles.indexOf(title);

          if (labelTitlePosition === -1) {
            console.log("Accepted label title is invalid.");
            continue;
          }

          scales[title] = d3.scaleOrdinal(d3.schemeCategory10)
              .domain(d3.range(dataNodes, (d) => d.labels[labelTitlePosition]));
        }

        let maxScales = {};

        // Define the scales for the value (numerical) attributes.
        defineNumericalScales();

        // Define the scales for the bar colors.
        let barColor = d3.scaleOrdinal(d3.schemeCategory10)
            .domain(valueTitles);

        // Define the action for the same scale button.
        d3.select("#sameScaleCheckbox")
            .on("change", handleOnSameScaleCheckboxChange);

        // Render the axes.
        let elementGroup = svg.append("g");
        let attributeGroups;
        drawAxes();

        // Render circles for the label (categorical) attributes and the value
        // (numerical) attributes.
        drawAttributes();

        // Setup edges info.
        let edgesInfo = data.edges;
        let dataEdges = edgesInfo.dataEdges;

        // Render edges.
        let edgesGroups = svg.append("g")
            .style("fill", "none")
            .style("stroke", "blue");

        drawDataEdges();

        function handleOnSortSelectionChange() {
          let sortMetric = this.value;

          dataNodes = elementGroup.selectAll("g")
              .sort((a, b) => {
                let sortLabelIndex;
                if (labelTitles.includes(sortMetric)) {
                  sortLabelIndex = labelTitles.indexOf(sortMetric);
                  return d3.ascending(a.labels[sortLabelIndex], b.labels[sortLabelIndex]);
                } else if (valueTitles.includes(sortMetric)) {
                  sortLabelIndex = valueTitles.indexOf(sortMetric);
                  return d3.ascending(a.values[sortLabelIndex], b.values[sortLabelIndex]);
                } else {
                  console.log("Invalid metric:", sortMetric);
                  return 0;
                }
              })
              .data();

          elementGroup.selectAll("g")
              .transition()
              .duration(1000)
              .style("transform", (d, i) => {
                return `rotate(${i / dataNodes.length}turn)`;
              });

          drawDataEdges();
        }

        function drawAxes() {
          // Render the attribute groups.
          attributeGroups = elementGroup.selectAll("g.attributeGroup");
          
          attributeGroups = attributeGroups
              .data(dataNodes)
              .enter()
            .append("g")
              .style("transform-origin", "50% 50%")
              .classed("attributeGroup", true)
              .merge(attributeGroups);

          attributeGroups.transition()
              .duration(1000)
              .style("transform", (d, i) => `rotate(${i / dataNodes.length}turn)`);

          // Render the axes.
          let centerRadius = (svgSideLength / 2) * (d3.select("#centerRadiusRange")
              .property("value") / 100);

          let attributeAxes = attributeGroups.selectAll("line.axisLine");

          attributeAxes = attributeAxes
              .data([0])
              .enter()
              .append("line")
              .classed("axisLine", true)
              .merge(attributeAxes);

          attributeAxes.attr("x1", graphOrigin)
              .attr("y1", graphOrigin - centerRadius)
              .attr("x2", graphOrigin)
              .attr("y2", graphOrigin - centerRadius - (radius * titlesUsed.length))
              .style("transform-origin", "50% 50%")
              .style("stroke", "black")
              .style("stroke-width", "1px");
        }

        function sortElements(metric) {
          dataNodes.sort(function(a, b) {
            let sortLabelIndex;
            if (labelTitles.includes(metric)) {
              sortLabelIndex = labelTitles.indexOf(metric);
              return d3.ascending(a.labels[sortLabelIndex], b.labels[sortLabelIndex]);
            } else if (valueTitles.includes(metric)) {
              sortLabelIndex = valueTitles.indexOf(metric);
              return d3.ascending(a.values[sortLabelIndex], b.values[sortLabelIndex]);
            } else {
              console.log("Invalid metric.", metric);
              return 0;
            }
          });
        }

        function handleOnClickAddRingButton() {
          titlesUsed.push(acceptedLabelTitles[0]);
          updateRings();
          defineNumericalScales();
          updateSortSelectionValues();
          drawAttributes();
        }

        function handleOnClickRemoveRingButton() {
          if (titlesUsed.length === 1) {
            return;
          }

          titlesUsed.pop();
          d3.selectAll(`.elements${titlesUsed.length}`)
              .remove();
          updateRings();
          defineNumericalScales();
          drawAttributes();
        }

        function updateRings() {
          let centerRadius = (svgSideLength / 2) * (d3.select("#centerRadiusRange")
              .property("value") / 100);

          radius = ((svgSideLength / 2) - centerRadius) / (titlesUsed.length);
          elementRadius = Math.min((centerRadius * Math.PI) / dataNodes.length, radius / 2);

          let rings = ringsGroup.selectAll("circle");

          rings = rings.data(d3.range(titlesUsed.length + 1))
              .enter()
              .append("circle")
              .merge(rings);

          rings.exit()
              .remove();

          rings.attr("cx", graphOrigin)
              .attr("cy", graphOrigin)
              .attr("r", (d, i) => (radius * i) + centerRadius)
              .style("fill", "none")
              .style("stroke", "black")
              .style("stroke-width", "1px");

          // List the ring options.
          let ringListOptions = ringList.selectAll("div")
              .data(titlesUsed);

          let ringListOption = ringListOptions.enter()
              .append("div");

          ringListOption.append("label")
              .text((d, i) => `Ring ${i}:`);

          let ringListSelectionSpan = ringListOption.append("span");
          let ringListSelection = ringListSelectionSpan.append("select")
              .attr("value", (d) => d)
              .on("change", handleOnRingSelectionChange);

          for (let title of acceptedLabelTitles) {
            ringListSelection.append("option")
                .text(title)
                .attr("value", title);
          }

          for (let title of acceptedValueTitles) {
            ringListSelection.append("option")
                .text(title)
                .attr("value", title);
          }

          ringListSelection.selectAll("option")
              .filter(function(d) {
                let value = d3.select(this)
                    .attr("value");
                return d === value;
              })
              .attr("selected", "");

          ringListOptions.exit().remove();
        }

        function updateSortSelectionValues() {
          let sortOptions = d3.select("#sortSelection")
              .selectAll("option")
              .data(d3.set(titlesUsed).values());

          sortOptions.exit()
              .remove();

          sortOptions = sortOptions.enter()
              .append("option")
              .merge(sortOptions)
              .attr("value", (d) => d)
              .text((d) => d);
        }

        function drawAttributes() {
          let centerRadius = (svgSideLength / 2) * (d3.select("#centerRadiusRange")
              .property("value") / 100);

          for (let rawTitleIndexInUsed in titlesUsed) {
            let titleIndexInUsed = Number(rawTitleIndexInUsed);
            let title = titlesUsed[titleIndexInUsed];

            if (acceptedLabelTitles.includes(title)) {
              let labelTitlePosition = labelTitles.indexOf(title);

              attributeGroups.each(function(d, i) {
                let elements = d3.select(this)
                    .selectAll(`.elements${titleIndexInUsed}`);
                    
                let elementsEnter = elements
                    .data([d])
                    .enter()
                    .append("circle")
                    .classed(`elements${titleIndexInUsed}`, true);

                let elementsTitle = elementsEnter.append("title");

                elements = elementsEnter.merge(elements);

                elements.attr("cx", graphOrigin)
                    .attr("cy", graphOrigin
                        - centerRadius
                        - (radius * titleIndexInUsed)
                        - elementRadius
                    )
                    .attr("r", elementRadius)
                    .style("fill", (d) => scales[title](d.labels[labelTitlePosition]))
                    .style("stroke-width", "2px")
                    .on("mouseover", handleOnMouseOverAttribute)
                    .on("mouseout", handleOnMouseOutAttribute);

                elements.exit().remove();

                elementsTitle.text((d) => `${d.labels[0]}\n${title}: ${d.labels[labelTitlePosition]}`);
              });
            } else if (acceptedValueTitles.includes(title)) {
              let valueTitlePosition = valueTitles.indexOf(title);

              attributeGroups.each(function(d, i) {
                let elements = d3.select(this)
                    .selectAll(`.elements${titleIndexInUsed}`);

                let selectedScales = d3.select("#sameScaleCheckbox")
                    .property("checked")
                    ? maxScales
                    : scales;
                    
                let elementsEnter = elements
                    .data([d])
                    .enter()
                    .append("rect")
                    .classed(`elements${titleIndexInUsed}`, true);

                elements = elementsEnter.merge(elements);

                elements.attr("x", graphOrigin - elementRadius)
                    .attr("y", (d) => {
                      return graphOrigin
                        - centerRadius
                        - (radius * titleIndexInUsed)
                        - selectedScales[title](d.values[valueTitlePosition]);
                    })
                    .attr("width", elementRadius * 2)
                    .attr("height", (d) => selectedScales[title](d.values[valueTitlePosition]))
                    .style("stroke-width", "2px")
                    .style("fill", barColor(title))
                    .on("mouseover", handleOnMouseOverAttribute)
                    .on("mouseout", handleOnMouseOutAttribute);

                elements.exit().remove();

                let elementsTitle = elementsEnter.append("title");

                elementsTitle.text((d) => `${d.labels[0]}\n${title}: ${d.values[valueTitlePosition]}`);
              });
            }
          }
        }

        function drawDataEdges() {
          let centerRadius = (svgSideLength / 2) * (d3.select("#centerRadiusRange")
              .property("value") / 100);

          let edgeLine = d3.lineRadial()
            .angle((d, i, a) => {
              if (i === 1) {
                return 0;
              }

              let nodeIndex = dataNodes.findIndex((value) => value.id === d);
              return (Math.PI * 2) * (nodeIndex / dataNodes.length);
            })
            .radius((d, i) => {
              if (i === 1) {
                return 0;
              }
              return centerRadius;
            })
            .curve(d3.curveBundle.beta(0.5));

          let edges = edgesGroups.selectAll("path")
              .data(dataEdges);

          let edgeStrokeOpacity = d3.scaleLinear()
              .domain([1, dataNodes.length / 2])
              .range([1, 0.2]);

          edges.enter()
              .append("path")
              .attr("transform", `translate(${graphOrigin}, ${graphOrigin})`);

          edgesGroups.selectAll("path")
              .each(function(d) {
                d3.select(this)
                    .attr("class", "")
                    .classed(`edge${d.src} edge${d.tgt}`, true);
              })
              .attr("d", (d) => {
                let dataEdgeIds = [d.src, -1, d.tgt];
                return edgeLine(dataEdgeIds);
              })
              .transition()
              .duration(1000)
              .style("stroke-opacity", (d) => {
                // Calculate the color opacity of the edge.
                let dataEdgeSrcPosition = dataNodes.findIndex((value) => value.id === d.src);
                let dataEdgeTgtPosition = dataNodes.findIndex((value) => value.id === d.tgt);

                let dataEdgePositionDist = Math.min(Math.abs(dataEdgeSrcPosition - dataEdgeTgtPosition),
                    dataNodes.length - Math.abs(dataEdgeSrcPosition - dataEdgeTgtPosition));

                return edgeStrokeOpacity(dataEdgePositionDist);
              });
        }

        function handleOnMouseOverAttribute(d) {
          d3.select(this.parentNode)
              .style("stroke", "black");

          d3.selectAll(`.edge${d.id}`)
              .classed("selected", true)
              .raise();
        }

        function handleOnMouseOutAttribute(d) {
          d3.select(this.parentNode)
              .style("stroke", "none");

          d3.selectAll(`.edge${d.id}`)
              .classed("selected", false);
        }

        function handleOnRingSelectionChange(d, i) {
          titlesUsed[i] = this.value;
          d3.selectAll(`.elements${i}`).remove();
          defineNumericalScales();
          drawAttributes();
          updateSortSelectionValues();
        }

        function defineNumericalScales() {
          let max = d3.max(dataNodes, (d) => {
            let currentMax = -Infinity;

            for (let title of titlesUsed) {
              if (!acceptedValueTitles.includes(title)) {
                continue;
              }

              let valueTitlePosition = valueTitles.indexOf(title);
              currentMax = Math.max(currentMax, d.values[valueTitlePosition]);
            }

            return currentMax;
          });

          for (let title of acceptedValueTitles) {
            let valueTitlePosition = valueTitles.indexOf(title);

            if (valueTitlePosition === -1) {
              console.log("Accepted value title is invalid:", title);
              continue;
            }

            scales[title] = d3.scaleLinear()
                .domain([0, d3.max(dataNodes, (d) => d.values[valueTitlePosition])])
                .range([2, radius]);

            maxScales[title] = d3.scaleLinear()
                .domain([0, max])
                .range([2, radius]);
          }
        }

        function handleOnSameScaleCheckboxChange() {
          drawAttributes();
        }

        function handleOnCenterRadiusRangeChange() {
          updateRings();
          defineNumericalScales();
          drawAxes();
          drawAttributes();
          drawDataEdges();
        }
      });
}

function handleOnDataSetSelectionChange() {
  svg.selectAll("*").remove();
  drawClusterVis();
}

main();
