# ClusterVis

ClusterVis is a technique for visualizing elements containing multiple dimensions.  The full paper, by Ricardo Cava, Carla M. Dal Sasso Freitas and Marco Winckler, could be found here: https://dl.acm.org/citation.cfm?id=3019612.3019684.  This is a replication of ClusterVis (not to be confused with the MGExplorer tool in the paper) for the University of San Francisco's CS 686 Research in Data Visualization class.

# How to Run

The code is written in JavaScript to utilize D3.js version 5.7.0, so any modern browser should be capable of viewing the page without any problems.  However, a local server will need to be instantiated in the root folder of this project in order to display the page properly.  Any tool that can perform this task can be used, though the simplest way is through Python 3.

1. Install Python 3
2. Run the following command in the root directory of this project: "python3 -m http.server 8080"
3. Load the following page from your browser: "localhost:8080"
