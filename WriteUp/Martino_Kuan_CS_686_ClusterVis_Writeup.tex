\documentclass[12pt, letterpaper]{article}

\usepackage{geometry}
\usepackage{graphicx}
\geometry{letterpaper, portrait, margin=1in}

\title{Implementation of ``ClusterVis: Visualizing Nodes Attributes in Multivariate Graphs"}
\author{Martino Kuan}
\date{October 27, 2018}

\begin{document}

\maketitle

\section{Scope}

The goal of the project was to implement ClusterVis, a visualization technique for rendering multivariate data \cite{cava2017clustervis}.  This means implementing the display of node attributes in a radial layout, with individual nodes occupying one portion of the entire circle.  The gaps between rings are reserved for user specified attributes of the nodes.  Related nodes are connected with edges.  Various interactivity features are also included, which are the ability to sort elements, select which attributes are represented in a ring, add and remove rings, highlight nodes and edges, display tooltips for nodes, and select the data set.

This project does not implement MGExplorer, which is a case study implementation that combines a node-link graph with ClusterVis.

\section{Implementation}

The project was implemented in JavaScript using the library D3.js version 5.4.0.  The authors were contacted for advice and permission to use the data set.  They agreed to provide the data set used in MGExplorer, which simplified the task of obtaining data for testing.  The data is included in the /ClusterVis/data folder of the project.

The first task was to get an idea of how ClusterVis works, so the project used the ClusterVis portion of MGExplorer to serve as the baseline for how the visualization should appear.  The "horizontal" axes, which will be referred to as rings, were the first pieces to render.  The attribute rings were encoded as circles, which allocate an equal amount of gap space between rings for each attribute.  Space in an additional center ring was reserved for later work on edges.  Next, work went into rendering the "vertical" axes, which represented individual nodes, which needed to be rotated such that each node occupied an equal amount of gap space.  Rotation was done using the rotate transformation functions of CSS.  The vertical axis was also offsetted to accommodate for the center ring.

The next task was to render the individual node attributes as colored circles for categorical data and bars for numerical data.  Since these two attribute types use different shapes, different scales had to be defined so that the categorical values would be distinguished by colors and that the numerical values would be depicted by the height of bars.  With bars, a value of zero still needed to be visible, so the numerical scales' output always had a minimum output of 1.  Additional scales for the bars were also defined in order to enable functionality of the "same scale" option.  Bar colors were also defined using the value's title.

After rendering the nodes and their attributes, the next task was to render the edges from the source node to the target node.  The edges were rendered using B-splines, with three control points located at the source node, the center of the visualization, and the destination node.  This is simplistic compared to the more complex curves seen in the original paper.  Figure \ref{fig:Simplistic_Edges} demonstrates the appearance of the edges.  In the paper, the curves were affected by the hierarchical relationship between nodes, which would have been more difficult to implement.  As for edge opacity, the distance between the nodes determined how transparent the edge should appear.  Edges appear more transparent the further the source and target nodes were from each other.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{Simplistic_Edges.png}
  \caption{These edges are simplistic, as the control points are located at the source node, the center of the visualization, and the target node.  They do not differentiate clusters nor depict the hierarchical relationships effectively.}
  \label{fig:Simplistic_Edges}
\end{figure}

Once the main components were completed, interactivity was the final and most difficult piece to implement.  Firstly, the code was written without regard for D3's enter, update, and exit functions, so a large amount of time was spent refactoring the code.  After refactoring, the mouse over aspect of the nodes was implemented.  This gave attributes a black border for the specified node, changed the color of connected edges to make it stand out, and displayed a tooltip containing information about the node and  the associated value of the attribute.  Afterwards, the user interface for controlling aspects of the visualization was worked on.  Many of these controls triggered an event upon changing their values, calling a custom handler function to update the visualization.  The selection options for sorting was implemented using the sort function of D3 selections.  Next, the same scale checkbox was added, which changed the scale used in numerical attributes and updated the visualization.  Next, the buttons for adding and removing rings, updated the entire visualization so that the attributes would appear in the correct place and that the gap space allocated to each ring was equally distributed.  An attribute selection for the rings would also be added or removed, where each selection would allow changing the attribute represented in a ring layer.  Changing of the data set is done using a selection box, where changing its value would remove the current ring layer and replace it with a completely new one.  A final additional piece of interactivity was implemented, which allows controlling the radius of the center ring.  The main interactivity feature missing from the original paper was the ability to resize the entire visualization window.  However, this was decided to be not as useful, as modern browsers allow zooming in mitigating this issue.  In addition, the visualization was not implemented inside of a window, which meant putting in additional effort for little gain.

ClusterVis also relies heavily on the concept of visualizing clusters, which means being able to visualize only a portion of the data set.  However, our implementation renders every single attribute on the screen at once, as shown in Figure \ref{fig:Medium_Data_Set}.  Part of this issue is that the implementation does not perform clustering on the nodes, nor does it understand the hierarchical relationship of the nodes.  As a result, nodes are not visualized as being members of some hierarchy.  There is currently no way to limit the elements to only the related nodes either.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{Medium_Data_Set.png}
  \caption{Even with a data set consisting of 92 nodes, it is already difficult to see the individual attributes and follow each individual edge.  The original paper would only depict a fraction of the elements seen here, based on which clusters the elements fall under.}
  \label{fig:Large_Data_Set}
\end{figure}

\section{Results}

This implementation contains most of the main features of ClusterVis.  Each node is separated into its own piece of the entire visualization.  Node attributes are represented using the proper shapes, and are placed according to the user's specification.  These attributes are also properly resized whenever the center radius changes or whenever the number of rings changes.  Highlighting of elements, as seen in Figure \ref{fig:Highlighting_Node}, and other user interactivity features, as seen in Figure \ref{fig:User_Controls}, Figure \ref{fig:Sorting}, and Figure \ref{fig:Before_After_Ring_Add}, are available to the user.  Simple edges are rendered with B-splines to depict node-to-node relationships, but fail to capture the hierarchical relationships.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{Highlighting_Node.png}
  \caption{Placing the mouse over a node attribute shows the tooltip, highlights all attributes of the same node, and changes the color of the connected edges to stand out from the other edges.  (Note that the cursor is not depicted due to the screenshot software used.)}
  \label{fig:Highlighting_Node}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{User_Controls.png}
  \caption{These are controls that allow the user to manipulate aspects of the visualization, such as the center ring's radius or the attribute represented in a specified ring layer.}
  \label{fig:User_Controls}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.75]{Sorted_By_Category.png}
  \includegraphics[scale=0.75]{Sorted_By_Qt_Publications.png}
  \caption{These two visualizations depict the same data set, with the left organized by category, while the right is sorted by the number of Qt publications, in ascending order.}
  \label{fig:Sorting}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.75]{Without_Degrees.png}
  \includegraphics[scale=0.75]{With_Degrees.png}
  \caption{This is the result of adding a new ring, then selecting the attribute representing the outermost ring to be "Degrees."}
  \label{fig:Before_After_Ring_Add}
\end{figure}

\section{Discussion}

This implementation of ClusterVis provides a rich interface to display multiple dimensions of data for each node.  Users have plenty of control over the visualization, shown in Figure \ref{fig:User_Controls}, and can inspect the values of each node's attributes.  In addition, node-to-node relationships are easily noticed when highlighting a node of interest, as seen in Figure \ref{fig:Highlighting_Node}.  Sorting simplifies the task of finding nodes with a specific value for an attribute, as seen in Figure \ref{fig:Sorting}.

However, this implementation lacks any way to filter or brush the data shown in the visualization.  The repercussions of not providing this feature is the increase in clutter as the number of nodes in a data set increases.  This is demonstrated in Figure \ref{fig:Data_Set_Sizes}, where the rightmost visualization has too many elements, which makes it unfeasible to decipher.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.5]{Small_Data_Set.png}
  \includegraphics[scale=0.5]{Medium_Data_Set_2.png}
  \includegraphics[scale=0.5]{Large_Data_Set.png}
  \caption{This is a depiction of three different data sets in order of increasing size, placed side-by-side for comparison.  The attributes and edges become much harder to see as the data set size increases due to the increased clutter.}
  \label{fig:Data_Set_Sizes}
\end{figure}

In addition, edge curves are not affected by hierarchical relationships, which makes this implementation not very useful for tracing ancestor-to-descendant relationships.

\section{Future Work}

Although many of the main features of ClusterVis were implemented, many of the current problems could be fixed.  Filtering or brushing of nodes could greatly improve the visibility of individual nodes.  A right-click interface to select a node cluster, similar to what is provided in MGExplorer, could give users the ability to brush nodes.  Hierarchical relationships could be implemented to give the node edges the proper shape, so that the hierarchical structure of nodes would become more visible.

\bibliography{Martino_Kuan_CS_686_ClusterVis_Writeup}
\bibliographystyle{ieeetr}

\end{document}
